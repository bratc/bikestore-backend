package com.ADSI1836648.Bike.repository;

import com.ADSI1836648.Bike.domain.Users;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<Users, Long> {
    Users findByUsername(String username);
}
