package com.ADSI1836648.Bike.repository;

import com.ADSI1836648.Bike.domain.Bike;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface BikeRepository extends JpaRepository<Bike, Integer> {
    Optional<Bike> findBySerial(String serial);

    //create query for serial and domain
    Iterable<Bike> findBySerialContaining(String serial);

    Iterable<Bike> findBySerialEquals(String serial);

    Iterable<Bike> findByModelContaining(String model);

    //get count bikes
    @Query(value = "select count(bike) from Bike bike")
    Integer countAllBikes();

    //get models like a name model
    @Query(value = "select bike from Bike bike where bike.model like %:model% ")
    List<Bike> findAllByModelQuery(@Param("model") String model);

    List<Bike> findAllByModel(String model);

}
