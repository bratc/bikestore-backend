package com.ADSI1836648.Bike;

import com.ADSI1836648.Bike.domain.DetailUser;
import com.ADSI1836648.Bike.domain.Users;
import com.ADSI1836648.Bike.domain.enumeration.Gender;
import com.ADSI1836648.Bike.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.boot.CommandLineRunner;

@SpringBootApplication
public class BikeApplication implements CommandLineRunner  {
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	final UserRepository userRepository;

	public BikeApplication(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(BikeApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		String password = "12345";

		Users user = new Users();
		DetailUser detailUser = new DetailUser();
		detailUser.setDocumentNumber("112233");
		detailUser.setLastName("ADMIN");
		detailUser.setName("Admin");
		user.setDetailUser(detailUser);
		user.setEmail("admin@localhost");
		user.setEnabled(true);
		user.setGender(Gender.MALE);
		user.setPassword(passwordEncoder.encode("12345"));
		user.setUsername("admin");
		// userRepository.save(user);
		for (int i = 0; i < 4; i++) {
			String passwordBcrypt = passwordEncoder.encode(password);
			System.out.println(passwordBcrypt);
		}

	}
}
