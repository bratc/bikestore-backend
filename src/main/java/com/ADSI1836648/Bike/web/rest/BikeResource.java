package com.ADSI1836648.Bike.web.rest;

import com.ADSI1836648.Bike.domain.Bike;
import com.ADSI1836648.Bike.domain.enumeration.ReportType;
import com.ADSI1836648.Bike.repository.BikeRepository;
import com.ADSI1836648.Bike.service.IBikeService;
import com.ADSI1836648.Bike.service.dto.BikeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class BikeResource {
    //guardar y crear una bicicleta
    @Autowired
    IBikeService bikeService;

    @PostMapping("/bikes")
    public ResponseEntity create(@RequestBody BikeDTO bikeDTO)  {
        return bikeService.create(bikeDTO);
    }

    // Get - obtener - read
    @GetMapping("/bikes")
    public Page<BikeDTO> read(@RequestParam(value = "pageSize") Integer pageSize,
                           @RequestParam(value = "pageNumber") Integer pageNumber,
                           @RequestParam(value = "sort.dir", required = false) String dir,
                           @RequestParam(value = "sort" ,required = false) String sort
    ) {
        return bikeService.read(pageSize, pageNumber, sort, dir);
    }

    // Put - actualizar - update
    @PutMapping("/bikes")
    public Bike update(@RequestBody Bike bike) {
        return bikeService.update(bike);
    }

    //Delete - borrar - delete
    @DeleteMapping("/bikes/{id}")
    public void delete(@PathVariable Integer id) {
        bikeService.delete(id);
    }

    @GetMapping("/bikes/{id}")
    public Optional<Bike> getById(@PathVariable Integer id){
        return bikeService.getById(id);
    }

    @GetMapping("bikes/search")
    public ResponseEntity search(@RequestParam(value = "serial", required = false) String serial,
                                 @RequestParam(value = "domain", required = false) String model){
        return bikeService.search(serial, model);
    }

    @GetMapping("bikes/validation-serial")
    public ResponseEntity validationExistSerial(@RequestParam(value = "serial", required = false) String serial) {
        return bikeService.validationExistSerial(serial);
    }

    //Get count of bikes
    @GetMapping("/bikes/count-query")
    public Integer countQuery(){
        return bikeService.countQuery();
    }

    @GetMapping("/bikes/get-model/{model}")
    public List<BikeDTO> getAllModelQuery(@PathVariable String model){
        return bikeService.getAllByModelQuery(model);
    }

    @GetMapping("/bikes/get-list")
    public List<BikeDTO> getAllList(){
        return bikeService.readList();
    }

    @GetMapping("/bikes/reporst/get-report")
    public void getReport(@RequestParam String model, HttpServletResponse response) throws Exception {
        response.setContentType("application/x-download");
        response.setHeader("Content-Disposition", String.format("attachment; filename=\"reporte.pdf\""));
        ReportType.PDF.generateJRXlsAbstractExporter(bikeService.generatePdf(model), response.getOutputStream());
    }


}


