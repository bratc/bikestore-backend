package com.ADSI1836648.Bike.web.rest;

import com.ADSI1836648.Bike.domain.Users;
import com.ADSI1836648.Bike.service.IUserService;
import com.ADSI1836648.Bike.service.UsuarioService;
import com.ADSI1836648.Bike.service.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UserResource {

    @Autowired
    IUserService userService;

     @Autowired
     UsuarioService usuarioService;



    @PostMapping("/users")
    public Users save(@RequestBody Users user){
        return userService.save(user);
    }

    @GetMapping("/users")
    public Iterable<Users> getUsers(){
        return userService.getAll();
    }

    @GetMapping("/user/account")
    public ResponseEntity<UserDTO> getUserAccount(@RequestParam(value = "username") String username){
        return new ResponseEntity<UserDTO>(userService.getUserAccount(username), HttpStatus.OK);
    }
}
