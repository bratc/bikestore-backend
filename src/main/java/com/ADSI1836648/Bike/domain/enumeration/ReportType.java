package com.ADSI1836648.Bike.domain.enumeration;


import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

import java.io.OutputStream;

/**
 * The ReportType enumeration.
 */
public enum ReportType {
    EXCEL(".xlsx","excel"){
        @Override
        public void generateJRXlsAbstractExporter(JasperPrint jasperPrint, OutputStream out) throws Exception  {
            JRXlsxExporter exporterXLS = new JRXlsxExporter();
            inOutParams(jasperPrint, out, exporterXLS);
            exporterXLS.exportReport();
        }
    },
    WORD(".docx","word"){
        @Override
        public void generateJRXlsAbstractExporter(JasperPrint jasperPrint, OutputStream out) throws Exception {
            JRDocxExporter exporterDocx = new JRDocxExporter();
            inOutParams(jasperPrint, out, exporterDocx);
            exporterDocx.exportReport();
        }
    },
    PDF(".pdf","pdf"){
        @Override
        public void generateJRXlsAbstractExporter(JasperPrint jasperPrint, OutputStream out) throws Exception {
            JRPdfExporter exporterPdf= new JRPdfExporter();
            inOutParams(jasperPrint, out, exporterPdf);
            exporterPdf.exportReport();
        }
    };

    private static void inOutParams(JasperPrint jasperPrint, OutputStream out, JRAbstractExporter exporter) {
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(out));
    }

    private String extension;
    private String reportType;

    ReportType(String extension, String reportType) {
        this.extension = extension;
        this.reportType = reportType;
    }

    public abstract void generateJRXlsAbstractExporter(JasperPrint jasperPrint, OutputStream out) throws Exception;

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public static ReportType getReportByType(String type){
        for (ReportType typeEnum: ReportType.values()){
            if (typeEnum.getReportType().equalsIgnoreCase(type)){
                return  typeEnum;
            }
        }
        throw new RuntimeException("Type not found!");
    }
}
