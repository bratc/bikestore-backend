package com.ADSI1836648.Bike.domain;

import com.ADSI1836648.Bike.domain.enumeration.Gender;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private Long id;

    private String fullName;

    //relation with detailUser
    @JoinColumn(name = "id_detail_user", unique = true)
    @OneToOne(cascade = CascadeType.ALL)
    private DetailUser detailUser;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    //For login
    @NotEmpty(message = "Este campo no puede estar vacio")
    @Size(min = 4, max = 20, message = "El tamaño del campo debe ser entre 4 y 20 caracteres")
    @Column(unique = true, length = 20)
    private String username;

    @NotEmpty(message = "Este campo no puede estar vacio")
    @Email(message = "el email no tiene la estructura correcta")
    @Column(unique = true)
    private String email;

    @NotEmpty(message = "Este campo no puede estar vacio")
    @Column(length = 60)
    private String password;

    private Boolean enabled;

    //relation with rols
    @ManyToMany
    @JoinTable(name = "user_has_rol",
            joinColumns = @JoinColumn(name = "id_user"),
            inverseJoinColumns = @JoinColumn(name = "id_rol"))
    private List<Rols> rols;

}
