package com.ADSI1836648.Bike.domain;

import com.ADSI1836648.Bike.domain.enumeration.TypeShockAbsorber;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Bike {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private int id;

    private String model;
    private Double price;
    private String serial;
    private Boolean status;

    @Lob
    @Column(name = "image")
    private byte[] image;

    @Column(name = "image_content_type")
    private String imageContentType;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_shock_absorber")
    private TypeShockAbsorber typeShockAbsorber;


    public String getSerial() {
        return serial;
    }
}
