package com.ADSI1836648.Bike.service;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


@Service
@Transactional
public class ReportService {
    private static final Logger logger = LoggerFactory.getLogger(ReportService.class);

    public JasperPrint getJasperPrintReport(final List<Object> dataSource, final Map<String, Object> parameters, final String jrxmlPath) {

        JasperPrint jasperPrint = null;

        try {
            JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
            JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(dataSource);
            jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
        } catch (JRException e) {
            logger.error("An error has occurred in the creation of the report: ", e);
        }

        return jasperPrint;
    }
}
