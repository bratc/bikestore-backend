package com.ADSI1836648.Bike.service.dto;


import com.ADSI1836648.Bike.domain.Users;

public class UserTransformer {

    public static UserDTO getUserDTOFromUsers(Users users){
        if(users == null){
            return null;
        }

        UserDTO dto = new UserDTO();

        dto.setId(users.getId());
        dto.setEmail(users.getEmail());
        dto.setEnabled(users.getEnabled());
        dto.setFirstName(users.getDetailUser().getName());
        dto.setLastName(users.getDetailUser().getLastName());
        dto.setUsername(users.getUsername());
        dto.setPassword(users.getPassword());
        dto.setRols(users.getRols());
        return dto;
    }

    public static Users getUsersFromUserDTO(UserDTO dto){
        if(dto == null){
            return null;
        }

        Users users = new Users();
        users.setId(dto.getId());
        users.setEmail(dto.getEmail());
        users.setEnabled(dto.getEnabled());
        users.setFullName(dto.getName()+ " " +dto.getLastName());
        users.setUsername(dto.getUsername());
        users.setPassword(dto.getPassword());
        users.setRols(dto.getRols());
        return users;

    }

}
