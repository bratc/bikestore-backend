package com.ADSI1836648.Bike.service.dto;

import com.ADSI1836648.Bike.domain.Rols;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class UserDTO implements Serializable {

    @Id
    private Long id;

    private String username;

    private String name;

    private String firstName;

    private String lastName;

    private String email;

    private String password;

    private Boolean enabled;

    private List<Rols> rols;

    private List<String> authorities;
}
