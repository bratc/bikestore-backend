package com.ADSI1836648.Bike.service;

import com.ADSI1836648.Bike.domain.Bike;
import com.ADSI1836648.Bike.service.dto.BikeDTO;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface IBikeService {

    public ResponseEntity create(BikeDTO bikeDTO);

    // Get - obtener - read
    public Page<BikeDTO> read(Integer pageSize, Integer pageNumber, String sort, String dir);

    // Put - actualizar - update
    public Bike update(Bike bike);

    //Delete - borrar - delete
    public void delete(Integer id);

    public Optional<Bike> getById(Integer id);

    public ResponseEntity search(String serial, String model);

    public ResponseEntity validationExistSerial(String serial);

    public Integer countQuery();

    public List<BikeDTO> getAllByModelQuery(String model);

    // Get - obtener - read
    public List<BikeDTO> readList();

public JasperPrint generatePdf(String model) throws IOException;
}
