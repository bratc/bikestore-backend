package com.ADSI1836648.Bike.service;

import com.ADSI1836648.Bike.domain.Users;
import com.ADSI1836648.Bike.service.dto.UserDTO;

public interface IUserService {

    public Users save(Users user);
    public UserDTO getUserAccount(String username);

    public Iterable<Users> getAll();
}
