package com.ADSI1836648.Bike.service;

import com.ADSI1836648.Bike.domain.Bike;
import com.ADSI1836648.Bike.repository.BikeRepository;
import com.ADSI1836648.Bike.service.dto.BikeDTO;
import com.ADSI1836648.Bike.service.transformer.BikeTransformer;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class IBikeServiceImp implements IBikeService {

    @Autowired
    BikeRepository bikeRepository;

    @Autowired
    ReportService reportService;


    @Override
    public ResponseEntity create(BikeDTO bikeDTO) {
        Bike bike = BikeTransformer.getBikeFromBikeDTO(bikeDTO);
        bike.setStatus(false);
        if (bikeRepository.findBySerial(bike.getSerial()).isPresent()) {
            return new ResponseEntity("El serial ya existe", HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity(BikeTransformer.getBikeDTOFromBIke(bikeRepository.save(bike)), HttpStatus.OK);
        }

    }

    @Override
    public Page<BikeDTO> read(Integer pageSize, Integer pageNumber, String sort, String dir) {
        Pageable pageable;
        if(sort != null) {
           pageable = PageRequest.of(pageNumber, pageSize, Sort.by(Sort.Direction.valueOf(dir.toUpperCase()),sort));
        } else {
            pageable = PageRequest.of(pageNumber, pageSize, Sort.by(Sort.Direction.ASC, "id"));
        }
        return bikeRepository.findAll(pageable)
                .map(BikeTransformer::getBikeDTOFromBIke);
    }

    @Override
    public Bike update(Bike bike) {
        
        return bikeRepository.save(bike);
    }

    @Override
    public void delete(Integer id) {
        bikeRepository.deleteById(id);
    }

    @Override
    public Optional<Bike> getById(Integer id) {
        return bikeRepository.findById(id);
    }

    @Override
    public ResponseEntity search(String serial, String model) {
        if(serial != null){
            return new ResponseEntity(bikeRepository.findBySerialContaining(serial), HttpStatus.OK);
        } else if (model != null){
            return new ResponseEntity(bikeRepository.findByModelContaining(model), HttpStatus.OK);
        } else return new ResponseEntity("Error in the request", HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity validationExistSerial(String serial) {
        if(serial != null){
            return new ResponseEntity(bikeRepository.findBySerialEquals(serial), HttpStatus.OK);
        } else {
            return new ResponseEntity("Error in the request", HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public Integer countQuery() {
        return bikeRepository.countAllBikes();
    }

    @Override
    public List<BikeDTO> getAllByModelQuery(String model) {
        return bikeRepository.findAllByModelQuery(model).stream().map(BikeTransformer::getBikeDTOFromBIke).collect(Collectors.toList());
    }

    @Override
    public List<BikeDTO> readList() {
        return bikeRepository.findAll().stream().map(BikeTransformer::getBikeDTOFromBIke).collect(Collectors.toList());
    }

    @Override
    public JasperPrint generatePdf(String model) throws IOException {
        JasperPrint jasperPrint = null;
        final Map<String, Object> parameters = new HashMap<>();
        parameters.put("dateStart", LocalDate.now().toString());
        String jrxmlPath = new ClassPathResource("templates/reports/ReporteAsistencia.jrxml").getFile().getPath();

        List<Bike> bikeList = bikeRepository.findAllByModel(model);

        try {
            jasperPrint = reportService.getJasperPrintReport(new ArrayList<>(bikeList), parameters, jrxmlPath);
        } catch (Exception e) {
            System.out.println("An error has occurred in the creation of the report: ");
        }

        return jasperPrint;
    }
}
