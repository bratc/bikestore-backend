package com.ADSI1836648.Bike.service;

import com.ADSI1836648.Bike.domain.Users;
import com.ADSI1836648.Bike.repository.UserRepository;
import com.ADSI1836648.Bike.service.dto.UserDTO;
import com.ADSI1836648.Bike.service.dto.UserTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class IUserServiceImp implements IUserService{

    @Autowired
    UserRepository userRepository;

    @Override
    public Users save(Users user) {
        return userRepository.save(user);
    }

    @Override
    public Iterable<Users> getAll() {
        return userRepository.findAll();
    }

/*
    public Users getUserAccount(String username) {
        Users user = userRepository.findByUsername(username);
        user.setPassword("");

       // UserDTO userDTO =  UserTransformer.getUserDTOFromUsers(user);
        ArrayList roles = new ArrayList<String>();
        user.getRols().forEach(item -> {
            roles.add(item.getName());
        });
        user.setRols(roles);
        return user;
    }*/

    @Override
    public UserDTO getUserAccount(String username) throws UsernameNotFoundException {
        Users user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("Error, username " + username + " no encontrado.");
        }

        UserDTO userDTO =  UserTransformer.getUserDTOFromUsers(user);
        ArrayList roles = new ArrayList<String>();
        userDTO.getRols().forEach(item -> {
            roles.add(item.getName());
        });
        userDTO.setPassword("");
        userDTO.setRols(null);
        userDTO.setAuthorities(roles);
        return userDTO;
    }
}
