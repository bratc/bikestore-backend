package com.ADSI1836648.Bike.auth;

public class JwtConfig {

    public static final String RSA_PRIVATE = "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEpAIBAAKCAQEA0Bbzi9+tvLjLdbUAm1rBU9BHR4POK5WPPGD3Sy2S2ebOnLkk\n" +
            "g60kuL+AXFNtXgf4N/+yhBMeY29z58DWFrWLyA8JLXgjBfdaMr2Wn746Z3bHSaWd\n" +
            "SIOo766f+nbU4FMfMpgJLr8+BCGbCKIZBEXaLwF3LoQEJNjudAUTFGNFHzshtx8v\n" +
            "YVz4aE2sp9fefsDrOraQJGMqvpolVh5nuhiFcumFVXTLsA9kVmrUP04clfiQWfLa\n" +
            "fzLyofFMiqsS5+paE1mifXdKqLTsqd+fXK6vSI6obryjnLDk7QAJZWsInqouNNog\n" +
            "iomf4ywMsWRWvNUAsjNA0Yt7Ao9N2s/2r9FZAwIDAQABAoIBAHehSkDWxj4iCy5P\n" +
            "cdrekRiLO/9cQjICWIeAw4rf13YYYB3vvpNI/jyMM9JgUBboBAbFpK6TgzvtPjg1\n" +
            "P59sncDVDBSs5Y2vh5Z2MlsB9Pe4bJflc4fV0VCCFwzaNkYE44GlsFnErnWXkFal\n" +
            "QjaXpfiyIf+G/A7m/CeNJ0voSjvKLHO+g8gKsgIvDyo+7l6jI0wQk3rulEVmGgcS\n" +
            "7Xepzu4gNl/FtiZ49MuZyPsjY4zg6l4Sec4vg/tzVvaGCtUEn+hup4Ud5w5wmsIP\n" +
            "xJynJiopc9mZLMtfHP5/Do2g8mLU6m9LtUKf0xBAFzCgyi/GYKGzJEITilACBk/V\n" +
            "alDGW2kCgYEA7iaR5uLqg7IQ5XwMGL7WBIGxoLLLse/p0jyDOE4XUeucu0KQ1Ar/\n" +
            "VMaWxFlLlSXO4PBKO+CSbqLvGZGUNGKjTXUiqeXtl6OTbPhyLT3Gsi6QEZrYpyFi\n" +
            "DjlwqVLaN/mFA2lNx0wrN8bW0X6u/j/3NikiIyOV5w7gbMyb2iguIv0CgYEA36+Y\n" +
            "vDxw7v66fv1xThYQFOyzKJ3mGrkGaoGXS4iuzJdAsQroAP4uEFwcWkeJeMtBO82B\n" +
            "npTHiunLLjKEaMbSaC5+gUDSh4aHiehZ+YVEtzzFltdH6WYTUoPjiiPXSBv0FMk1\n" +
            "vawbk8B5yjuoMUyrILzpQlW7mpKpKaZgj4isK/8CgYEAyPn9yRaYiJleTvw3jS6H\n" +
            "mHgG+jHJWxm3freYmYy1e2nV8+ZCKOA7CEUrePpOocitZQED9LIZgYq5Mx+7LdZM\n" +
            "MBL5NN4Yew8NXDnyySFILJjE9kZyLeErSgvJeuuzwuctDGryu40ZL9H6+gLqFSDQ\n" +
            "NIHUEEl4uOzCEq89m4arINUCgYEAv8CzKSKTGX4Kd9Rh7jOho55p0zbQzDWy11iy\n" +
            "TxEj78T1sw+LZVaAPIwJzianJYLwMAjyxfP6vw2+nKK3uW4/bwn0bm8YSw+7XzBs\n" +
            "+ScSihYxuCZiwiU8CS+AvqoM4gb8jVkTlb5VgaqtfPz5KRH1XUI0oCYIhfCVzVuQ\n" +
            "SVs7ZRcCgYBCx08CDOTqc3kshuLpqvU+jLWRxhh335AxFOrwli6MfX+ihTSNHySC\n" +
            "0HQD0mQQA4TodUnVBuVwmmM0SoDoESZvQeCu/d+DKWt0T0bfL+DHcldTbk60EQgd\n" +
            "b9QzSdOoC5G5IVH5dvaqwp8uwPbCM6h832HmonnPiK96AI5nX9hn+A==\n" +
            "-----END RSA PRIVATE KEY-----";

    public static final String RSA_PUBLIC = "-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0Bbzi9+tvLjLdbUAm1rB\n" +
            "U9BHR4POK5WPPGD3Sy2S2ebOnLkkg60kuL+AXFNtXgf4N/+yhBMeY29z58DWFrWL\n" +
            "yA8JLXgjBfdaMr2Wn746Z3bHSaWdSIOo766f+nbU4FMfMpgJLr8+BCGbCKIZBEXa\n" +
            "LwF3LoQEJNjudAUTFGNFHzshtx8vYVz4aE2sp9fefsDrOraQJGMqvpolVh5nuhiF\n" +
            "cumFVXTLsA9kVmrUP04clfiQWfLafzLyofFMiqsS5+paE1mifXdKqLTsqd+fXK6v\n" +
            "SI6obryjnLDk7QAJZWsInqouNNogiomf4ywMsWRWvNUAsjNA0Yt7Ao9N2s/2r9FZ\n" +
            "AwIDAQAB\n" +
            "-----END PUBLIC KEY-----";
}
